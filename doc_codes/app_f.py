import io
import json
import os
import re
import shutil  # For copying files and for file handling.
import time
import pandas as pd
from flask import (Flask, redirect, render_template, request,jsonify,
                   send_from_directory, url_for,flash)
from pdfminer.converter import (HTMLConverter, PDFPageAggregator,
                                TextConverter, XMLConverter)
from pdfminer.layout import LAParams, LTTextBox
from pdfminer.pdfinterp import PDFPageInterpreter, PDFResourceManager
from pdfminer.pdfpage import PDFPage
from werkzeug.utils import secure_filename

from doc_codes.doc_sum_test import Prediction
from doc_codes.doc_sum_train import Doc_summ

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = './UPLOAD_FOLDER/'

global BatchList
global TextList
global BatchCount
global TextCount
global Log
BatchList = []
TextList = []
BatchCount = 0
TextCount = 0
Log = {}

@app.route('/')
def index():
	# return render_template('Page.html')
    
    Console = "None"
    ProgressBarValue = 0

    if 'UPLOAD_FOLDER' in os.listdir('./'):
    
        for fileName in os.listdir(app.config['UPLOAD_FOLDER']):
            shutil.rmtree(os.path.join(app.config['UPLOAD_FOLDER'], fileName))
            
        os.rmdir(app.config['UPLOAD_FOLDER'])
        
    os.mkdir(app.config['UPLOAD_FOLDER'])

    return render_template('Page.html', BatchList = BatchList, Console = Console, ProgressBarValue = ProgressBarValue)

@app.route('/RunPage', methods = ['GET', 'POST'])
def RunPage():
    global BatchList
    global TextList
    global TextCount
    global BatchCount
    global summary_length
    global Log
    
    errors = []
    result = None

    summarized_result={}
    summary_length=None

    SelectedItem = request.form.get("Selection")

    # try:
    
    # except:
    #     errors.append("Unable to get text/Summary length")
    #     # return render_template('Page.html',errors=errors)

    def getLocalTime():
        LocalTime = time.localtime()
        return "{}/{}/{} {}:{}:{}".format(LocalTime.tm_mday, LocalTime.tm_mon, LocalTime.tm_year, LocalTime.tm_hour, LocalTime.tm_min, LocalTime.tm_sec)
    
    def getLogTime():
        LocalTime = time.localtime()
        return "{}{}{}_{}{}{}".format(LocalTime.tm_mday, LocalTime.tm_mon, LocalTime.tm_year, LocalTime.tm_hour, LocalTime.tm_min, LocalTime.tm_sec)
    
    if request.form.get("AddBTN"):
        
        BatchCount += 1
        
        logTime = getLogTime()
        
        BatchList = [["Batch_" + str(BatchCount) +"_"+logTime, "Status: New"]] + BatchList
        
        Log["Batch_" + str(BatchCount) +"_"+logTime] = "Batch: Batch_{}\n\tBatch Created at: {}".format(str(BatchCount) +"_"+logTime, logTime)
        
        os.mkdir(os.path.join(app.config['UPLOAD_FOLDER'], "Batch_" + str(BatchCount) +"_"+logTime))
        os.mkdir(os.path.join(app.config['UPLOAD_FOLDER'], "Batch_" + str(BatchCount) +"_"+logTime, "uploads"))
        
        Console = "New Batch: Batch_{} Added!".format("Batch_" + str(BatchCount) +"_"+logTime)
    
        return render_template('Page.html', BatchList = BatchList, Console = Console, ProgressBarValue = 0)


    if SelectedItem != None:
        
        batchStatus = [i[1] for i in BatchList if i[0] == SelectedItem][0]

        if request.form.get("SummarizeBlock") and request.form.get["summarizer_length"]:
            # Log[SelectedItem] += "\n\t\tProcessing Text..."
            # Log[SelectedItem] += "\n\t\tStart time: {}".format(getLocalTime())

            text = request.form['SummarizeBlock']
            summary_length = request.form['summarizer_length']
        
            # try:
            Pred = Prediction(text,summary_length)
            Text_Extract = Pred.summary()
            boolSelectCode = True
                # summarized_result = {
                #     "output": Text_Extract,
                #     }
                # summarized_result = {str(key): value for key, value in summarized_result.items()}
                # print(summarized_result)
                # summarized_result = summarized_result['output']
            result = Text_Extract

            # except:
            #     errors.append("Requested summary length is greater than given text length ")

            return render_template('Page.html',summarized_result=result, errors=errors)

        elif request.form.get("DeleteBTN"):
            Console = "Batch: {} Deleted!".format(SelectedItem)
            
            shutil.rmtree(os.path.join(app.config['UPLOAD_FOLDER'], SelectedItem))
            BatchList = [i for i in BatchList if i[0] != SelectedItem]
            Log[SelectedItem] = "Deleted"
        
        elif request.form.get("UploadBTN"):
            if batchStatus == "Status: New":
                for index, item in enumerate(BatchList):
                    if item[0] == SelectedItem:
                        BatchList[index] = [SelectedItem, "Status: Uploading Files..."]

                uploaded_file_list = request.files.getlist('file')
                
                if uploaded_file_list != []:
                
                    Log[SelectedItem] += "\n\tUpload request: {} Files".format(len(uploaded_file_list))
                    Log[SelectedItem] += "\n\t\tUploading Files..."
                    Log[SelectedItem] += "\n\t\tStart time: {}".format(getLocalTime())
        
                    for file in uploaded_file_list:
                        filename = secure_filename(file.filename)
                        file.save(os.path.join(app.config['UPLOAD_FOLDER'], SelectedItem, "uploads", filename))
                                
                    for index, item in enumerate(BatchList):
                        if item[0] == SelectedItem:
                            BatchList[index] = [SelectedItem, "Status: Upload Complete"]
                            
                    Log[SelectedItem] += "\n\t\tEnd time: {}".format(getLocalTime())
                    Console = "Uploaded: "  + ', '.join([i.filename for i in uploaded_file_list]) + ". SelectedItem: " + str(SelectedItem)
                    
                else:
                    Console = "No file selected for upload!"
            else:
                Console = "Selected Batch is not a New Batch!"
                
        elif request.form.get("ProcessBTN"):
            if batchStatus == "Status: Upload Complete":
                for index, item in enumerate(BatchList):
                    if item[0] == SelectedItem:
                        BatchList[index] = [SelectedItem, "Status: Processing..."]
                Console = "Processing requested for {}!".format(SelectedItem)
                
                fileList = os.listdir(os.path.join(app.config['UPLOAD_FOLDER'], SelectedItem, "uploads"))

                df = pd.DataFrame({"File Name":[],"Text_Extract":[]})

                Log[SelectedItem] += "\n\tProcess request: {} Files".format(len(fileList))
                Log[SelectedItem] += "\n\t\tProcessing Files..."
                Log[SelectedItem] += "\n\t\tStart time: {}".format(getLocalTime())

                dfCount = 0

                for fileName in fileList:
                    if fileName.split('.')[1] == "pdf":
                        Pages = {}
                        Page_n = 0
                        pdfFile = os.path.join(app.config['UPLOAD_FOLDER'], SelectedItem, 'uploads', fileName)
                        
                        # PDFMiner_Components
                        rsrcmgr = PDFResourceManager()
                        retstr = io.StringIO()

                        laparams = LAParams()
                        device = TextConverter(rsrcmgr, retstr, laparams=laparams)
                        interpreter = PDFPageInterpreter(rsrcmgr, device)

                        with open(pdfFile, 'rb') as fp:
                            for page in PDFPage.get_pages(fp):
                                interpreter.process_page(page)
                                Pages[Page_n] =  retstr.getvalue()
                                Page_n+=1
                        
                        text = Pages[0] #By default, we are resorting to the first page
                        # print(text)
                        if len(text) > 27: # 27 randomly selected to confirm validity of text extracted!
                            try:
                                Pred = Prediction(text,summary_length)
                                Text_Extract = Pred.summary()
                                boolSelectCode = True
                                summarized_result = {
                                    "Text_Extract": Text_Extract
                                    }
                                summarized_result = {str(key): value for key, value in summarized_result.items()}
                                print(summarized_result)
                                # summarized_result = summarized_result['output']
                                boolSelectCode = True
                                Log[SelectedItem] += "\n\t\t\tFile: {}; Identified and Processed!".format(fileName)
                            except:
                                pass
                        else:
                            Log[SelectedItem] += "\n\t\t\tFile: {}; ERROR - Not Identified by any present Template!".format(fileName)
                            summarized_result = {}

                        summarized_result['File Name'] = fileName
                        df.loc[dfCount] = summarized_result
                        dfCount += 1
                Log[SelectedItem] += "\n\t\tEnd time: {}".format(getLocalTime())

                writer = pd.ExcelWriter(os.path.join(app.config['UPLOAD_FOLDER'], SelectedItem, 'Extract.xlsx'), engine='xlsxwriter')
                if (df.shape[0] != 0):
                    df.to_excel(writer, index = False, sheet_name="Sheet 1")
                else:
                    pass
                writer.save()

                for index, item in enumerate(BatchList):
                    if item[0] == SelectedItem:
                        BatchList[index] = [SelectedItem, "Status: Processed"]
            else:
                Console = "Selected Batch is already processed!"
            
        elif request.form.get("ReportBTN"):
            Console = "Log requested for {}!".format(SelectedItem)
            
            Log[SelectedItem] += "\n\tLog Requested: {}".format(getLocalTime())
            
            with open("./static/log.txt", 'w') as f:
                f.write(Log[SelectedItem])
            return send_from_directory('./static', 'log.txt', as_attachment=True)

        elif request.form.get("DownloadBTN"):
            if batchStatus == "Status: Processed":
                Console = "Download requested for {}!".format(SelectedItem)
                
                Log[SelectedItem] += "\n\t\tDownload Request: {}".format(getLocalTime())
                
                return send_from_directory(os.path.join(app.config['UPLOAD_FOLDER'], SelectedItem), 'Extract.xlsx', as_attachment=True)
            else:
                Console = "Selected Batch is not processed yet!"
        else:
            Console = "None"
            
        ProgressBarValue = 60

        return render_template('Page.html', summarized_result=result, errors=errors, BatchList = BatchList, Console = Console, ProgressBarValue = ProgressBarValue)
        
    else:
        return render_template('Page.html', summarized_result=result, errors=errors, BatchList = [], Console = "No batches present or none selected!", ProgressBarValue = 0)


if __name__ == '__main__':
   app.run(debug = True)
