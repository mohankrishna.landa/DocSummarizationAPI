#import required library
import nltk
import numpy as np
import pandas as pd
from nltk.tokenize import sent_tokenize

nltk.download('punkt',quiet=True) # one time execution
import json
import re

import pdfplumber
#importing packeges from Glove library
from glove import Corpus, Glove

from .doc_sum_train import Doc_summ


class Prediction(Doc_summ):
    def __init__(self,text,summary_length,json_path="data.json"):
        self.text = text
        self.json_file = json_path
        super().__init__(self.json_file)
        if summary_length!=None:
            self.length_summary = int(summary_length)
        else:
            self.length_summary = 5
        self.predictFile_path = self.data['training_data']['predictFile_path']
        # if self.predictFile_path.endswith(".pdf"):
        #     self.predict_text=self.readPDFPlumberFile(self.predictFile_path)
        #     self.writeText(self.text,self.predictFile_path)
        # else:
        #     self.predict_text = self.readTextFile(self.text)
        self.writeText(self.text,self.predictFile_path)
        self.predict_text = self.readTextFile(self.predictFile_path)
        # print(self.predict_text)
        self.predict_sentences=self.cleanSentences(self.predict_text)
        # remove stopwords from the prediction sentences
        self.predict_sentences = [self.removeStopwords(r.split()) for r in self.predict_sentences]

        self.word_embeddings = pd.read_pickle(self.data['training_data']['embeddings'])
        
        self.sentenceVectors()
        self.ranked_sentences = self.rankSentences()
        # output = self.summary()
    #extract sentence vector
    def sentenceVectors(self):
        global sentence_vectors
        sentence_vectors = []
        for i in self.predict_sentences:
            if len(i) != 0:
                v = sum([self.word_embeddings.get(w, np.zeros((100,))) for w in i.split()])/(len(i.split())+0.001)
            else:
                v = np.zeros((100,))
            sentence_vectors.append(v)
        return sentence_vectors

    def rankSentences(self):
        #finding simalarity between sentences using cosine simalarity
        # similarity matrix
        sim_mat = np.zeros([len(self.predict_sentences), len(self.predict_sentences)])
        # import libray and find out page rank over 
        
        # finding sentences rank over graph
        nx_graph = nx.from_numpy_array(sim_mat)
        scores = nx.pagerank(nx_graph)
        #sorting sentences rank wise
        ranked_sentences = sorted(((scores[i],s) for i,s in enumerate(self.predict_sentences)), reverse=True)
        return ranked_sentences


    # Specify number of sentences to form the summary
    def summary(self):
        return "".join([(self.ranked_sentences[i][1]) for i in range(self.length_summary)])
