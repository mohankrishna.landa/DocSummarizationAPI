#import required library
import json

import nltk
import numpy as np
import pandas as pd
from nltk.tokenize import sent_tokenize
from pydantic import FilePath

nltk.download('punkt',quiet=True) # one time execution
import re

import pdfplumber
#importing packeges from Glove library
from glove import Corpus, Glove


class Doc_summ:
    def __init__(self,json_path="data.json"):
        #input file path
        self.json_file = json_path
        self.data = self.read_data()
        self.trainingFile_path=self.data['training_data']['trainingFile_path']
        self.stopWords()
        self.train_text=self.readPDFPlumberFile(self.trainingFile_path)
        self.train_sentences=self.cleanSentences(self.train_text)
        self.train_sentences = [self.removeStopwords(r.split()) for r in self.train_sentences]
        self.train_sentences=self.trainingSentences(self.train_sentences)
        # creating a corpus object
        self.corpus = Corpus()
        #training the corpus to generate the co occurence matrix which is used in GloVe
        self.corpus.fit(self.train_sentences, window=10)

        self.word_list = self.creatWordlist(self.train_sentences)
        # self.glove = self.training_model()
        # self.transferLearning()

    def read_data(self):
        with open(self.json_file) as f:
            data = json.load(f)
        return data

    #Reading the file and extracting text
    def readPDFPlumberFile(self,file_path):
        global text
        pdf=pdfplumber.open(file_path)
        text=''
        for i in range(len(pdf.pages)):
            page = pdf.pages[i]
            text_final = page.extract_text()
            text+=text_final
            return text
            
    def writeText(self,text,file_path):
        with open(file_path,'w+') as f:
            f.write(text)
        f.close()

    #Reading the file and extracting text
    def readTextFile(self,file_path):
        text = ''
        with open(file_path,'r') as f:
            for line in f:
                text+=line
            return text

    #getting clean sentences list of data
    def cleanSentences(self,text):
        global clean_sentences
        sentences = [s for s in sent_tokenize(text)]
        #data cleaning proccess
        # remove punctuations, numbers and special characters
        clean_sentences = pd.Series(sentences).str.replace("[^a-zA-Z]", " ")

        # make alphabets lowercase
        clean_sentences = [s.lower() for s in clean_sentences]
        return clean_sentences

    #download and getting stop_words list
    def stopWords(self):
        global stop_words
        nltk.download('stopwords')
        from nltk.corpus import stopwords
        stop_words = stopwords.words('english')
    
    # function to remove stopwords
    def removeStopwords(self,sen):
        sen_new = " ".join([i for i in sen if i not in stop_words])
        return sen_new
        # remove stopwords from the sentences

    # # download pretrained GloVe word embeddings
    # ! wget http://nlp.stanford.edu/data/glove.6B.zip

    def vectorExtraction(self):
        global word_embeddings
        # Extract word vectors from pretrainined model
        word_embeddings = {}
        f = open(self.data['training_data']['glove_con_path'], encoding='utf-8')
        for line in f:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            word_embeddings[word] = coefs
        f.close()
    
    #getting sentences to trained model on our data set
    def trainingSentences(self,text):
        new=[line.split(" ") for line in text]
        return new
    

    #getting word list to tansfer learning
    def creatWordlist(self,train_sentences):
        # global word_list
        word_list=[]
        for i in range(len(train_sentences)):
            for j in range(len(train_sentences[i])):
                word_list.append(train_sentences[i][j])
        return word_list

    def training_model(self):
        #creating a Glove object which will use the matrix created in the above lines to create embeddings
        #We can set the learning rate as it uses Gradient Descent and number of components
        glove = Glove(no_components=100, learning_rate=0.05)
        glove.fit(self.corpus.matrix, epochs=20, no_threads=4, verbose=True)
        glove.add_dictionary(self.corpus.dictionary)
        glove.save(self.data['training_data']['glove_model_path'])
        return glove

    #transfer learning from our dataset to pretrained 
    def transferLearning(self,_glove):
        for i in self.word_list:
            if i in word_embeddings:
                word_embeddings[i]=_glove.word_vectors[self.glove.dictionary[i]]
            else:
                word_embeddings.update({i:_glove.word_vectors[self.glove.dictionary[i]]})
        return word_embeddings
