import io
import json
import os
import re
import shutil  # For copying files and for file handling.
import time
import pandas as pd
from flask import (Flask, redirect, render_template, request,jsonify,
                   send_from_directory, url_for,flash)
from pdfminer.converter import (HTMLConverter, PDFPageAggregator,
                                TextConverter, XMLConverter)
from pdfminer.layout import LAParams, LTTextBox
from pdfminer.pdfinterp import PDFPageInterpreter, PDFResourceManager
from pdfminer.pdfpage import PDFPage
from werkzeug.utils import secure_filename

from doc_codes.doc_sum_test import Prediction
from doc_codes.doc_sum_train import Doc_summ

app = Flask(__name__)

UPLOAD_FOLDER = 'output_reviews_folder/'
REVIEWS_FOLDER = 'product_reviews_folder/'
# from app import app
app = Flask(__name__)
app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['REVIEWS_FOLDER'] = REVIEWS_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif','xlsx','csv'])

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/RunPage', methods = ['GET', 'POST'])
def RunPage():
    global BatchList
    global TextList
    global TextCount
    global BatchCount
    global Log
    
    errors = []
    results = {}

    summarized_result=None
    # SelectedItem = request.form.get("Selection")
    if request.method=='POST':
        data = json.loads(request.data,strict=False)

        # if data.get("analysis_type")==".txt":

        try:
            text = data.get("text",None)

            summary_length = data.get("summary_length",1)
        except:
            errors.append("Unable to get text/Summary length")
            return jsonify(dict({"error":errors}))
        
        try:
            Pred = Prediction(text,summary_length)
            Text_Extract = Pred.summary()
            boolSelectCode = True
            result = {
                "output": Text_Extract,
                }
            result = {str(key): value for key, value in result.items()}
            # print(result)
            summarized_result = result['output']
            # summarized_result = Text_Extract

        except:
            errors.append("Requested summary length is greater than given text length ")
            return jsonify(dict({"error":errors}))

    return jsonify(dict({"summarized_text":summarized_result}))

if __name__ == '__main__':
   app.run(debug = True)
